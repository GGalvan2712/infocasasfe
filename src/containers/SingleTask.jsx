import { faCheckSquare, faMinusSquare } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React , {useState , useEffect} from "react";
import { useParams } from "react-router";
import { getSingleTask, updateTask } from "../actions/actions";

const SingleTask = () => {
  
  let { id } = useParams();
  
  const [useTask , setTask] = useState(null);

  useEffect(() => {
      loadTask()
  },[])
  
  const loadTask = async () => {
    
    let response = await getSingleTask(id);
    
    if(response.success){
        setTask(response.data);
        return true;
    }else{
       throw new Error("Error al cargar la tarea");
    }
}

const handleChangeStatus = async (id , status) => {

  let response = await updateTask(id , status);
  
  if(response.success){
    loadTask();
    return true;
  }else{
    throw new Error("Error al actualizar la tarea");
  }

};

  return (
    <div className="container h100">
      <div className="card h100 text-center">
        <div className="card-header my-auto">Tarea : {useTask ? useTask.name : '' } &nbsp;
        {useTask ? useTask.completed_at ? "Completa el: " + useTask.completed_at : 'No completada' : ''} &nbsp;
        {useTask ?  useTask.completed ? <i className="successIcon" onClick={ () => handleChangeStatus(useTask.id , useTask.completed) }><FontAwesomeIcon icon={faCheckSquare} /></i> : <i className="errorIcon" onClick={ () => handleChangeStatus(useTask.id , useTask.completed) }><FontAwesomeIcon icon={faMinusSquare}/></i> : '' }
        </div>
        
      </div>
    </div>
  );
};

export default SingleTask;
