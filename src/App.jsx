import React from 'react';
import Header from './components/header/Header';
import './App.css'

const App = () => {
   return (
   
    <div className="container-fluid">
        <Header />
    </div>

   );
};

export default App;