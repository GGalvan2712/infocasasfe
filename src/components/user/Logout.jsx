import React , {useEffect} from "react";
import { logout } from "../../actions/actions";

const Logout = () => {

    useEffect(() => {
       logout();
    }, [])

    return(
        <div className="container">
            <h1>Hasta Luego!</h1>
        </div>
    )
}

export default Logout;