import React , { useState} from 'react';
import './Login.css';
import { login } from '../../actions/actions';

const Login = () => {

    const [useEmail , setEmail ] = useState(null);
    const [usePassword , setPassword] = useState(null);

    const handleLogin = async (e) => {

        e.preventDefault();
        e.stopPropagation();
        console.log(useEmail);
        console.log(usePassword);
        let formData = new FormData();
        formData.append('email' , useEmail);
        formData.append('password' , usePassword);
        
        await login(formData);
        
    };

    return(
        <div className="container">
            <div className="row row-login">
                <div className="col-4 offset-4 my-auto">
                    <form className="form-signin text" onSubmit={handleLogin}>
                        <div className="col text-center">
                            <img className="mb-4" src="/logo192.png" alt="" width="72" height="72"></img>
                            <h1 className="h3 mb-3">Login</h1>
                         </div>
                        
                        <div className="form-group">
                            <input type="email" className="form-control" placeholder="Email address" onChange={e => setEmail(e.target.value)} required></input>
                        </div>
                        <div className="form-group">
                            <input type="password" className="form-control" placeholder="Password" onChange={ e => setPassword(e.target.value) } required></input>
                        </div>
                        
                        <button className="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
                    </form>
                </div>
            </div>
        </div>
    )
}

export default Login;