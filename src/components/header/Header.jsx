import React from 'react';
import Logout from '../user/Logout';
import Login from '../user/Login';
import Inicio from '../Inicio';
import {BrowserRouter as Router , Route , Link , Switch , Redirect } from 'react-router-dom';
import SingleTask from '../../containers/SingleTask';

const Header = () => {

   return (
        <Router>
            <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
                <Link className="navbar-brand" to="/">
                    <img src="/logo192.png" width="30" height="30" className="d-inline-block align-top" alt=""></img>
                    Navbar
                </Link>
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse justify-content-end" id="navbarNav">
                    <ul className="navbar-nav">
                        <li className="nav-item">
                            <Link to="/login">Login</Link>
                        </li>
                        <li className="nav-item">
                            <Link to="/logout">Logout</Link>
                        </li>
                    </ul>
                </div>
            </nav>
            <Switch>
                <Route exact path="/">
                   { localStorage.getItem('access_token') == null ? <Redirect exact to="/login" /> :  <Inicio />}
                </Route>
                <Route exact path="/login">
                   { localStorage.getItem('access_token') != null ? <Redirect exact to="/" /> : <Login />} 
                </Route>
                <Route exact path="/task/:id" component={SingleTask} />
                <Route exact path="/logout" component={Logout} />
            </Switch>
        </Router>
    );
}


export default Header;