import React, { useEffect, useState } from "react";
import { deleteTask, getTasks , newTask, updateTask } from "../actions/actions";
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import { faCheckSquare, faEye, faMinusSquare, faTrashAlt } from "@fortawesome/free-solid-svg-icons";
import { Link } from "react-router-dom";

const Inicio = () => {
  const [useNewTask, setNewTask] = useState(null);
  const [useTasks, setTasks] = useState(null);
  const [useSearch , setSearch] =useState(null);

  useEffect(() => {
    getAllTasks()
  }, []);

  const getAllTasks = async () => {
    let getList = await getTasks();
    
    if (getList.success) {
      setTasks(getList.data);
      return true;
    }

    throw new Error("Hubo un error al traer las tareas");
  };

  const handleNewTaskValue = (e) => {
    setNewTask(e.target.value);
  };

  const handleNewTask = async () => {
      
    let nuevaTarea = new FormData();
    nuevaTarea.append('name' , useNewTask);
    let response = await newTask(nuevaTarea);
    
    if(response.success){
       getAllTasks();
       return true;
    }else{
        throw new Error("Error al crear la tarea");
    }
  };

  const handleChangeStatus = async (id , status) => {

    let response = await updateTask(id , status);
    
    if(response.success){
      getAllTasks();
      return true;
    }else{
      throw new Error("Error al actualizar la tarea");
    }
  
  };

  const handleDeleteTask = async (id) => {

    let response = await deleteTask(id);

    if(response.success){
      getAllTasks();
      return true;
    }else{
      throw new Error("Error al eliminar la tarea");
    }
  
  }

  const handleSearch = async () => {
    let getList = await getTasks(useSearch);
    console.log(getList);
    if (getList.success) {
      setTasks(getList.data);
      return true;
    }

    throw new Error("Hubo un error al traer las tareas");
  }


  return (
    <div className="container">
      <div className="row">
        <div className="col-6 offset-3 my-auto">
          <div className="row">
            <div className="col-8 offset-2">
              <h1> Lista de Tareas </h1>
            </div>
          </div>

          <div className="row">
            <div className="col-8 offset-2">
              <div className="input-group mb-3">
                <input
                  type="text"
                  className="form-control"
                  onChange={handleNewTaskValue}
                  placeholder="Crear una tarea"
                ></input>
                <div className="input-group-append">
                  <button
                    className="btn btn-outline-secondary"
                    onClick={handleNewTask}
                    type="button"
                  >
                    Crear
                  </button>
                </div>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-8 offset-2">
              <div className="input-group mb-3">
                <input
                  type="text"
                  className="form-control"
                  onChange={(e) => setSearch(e.target.value)}
                  placeholder="Crear una tarea"
                ></input>
                <div className="input-group-append">
                  <button
                    className="btn btn-outline-secondary"
                    onClick={handleSearch}
                    type="button"
                  >
                    Crear
                  </button>
                </div>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-10 offset-1">
              <table className="table table-dark">
                <thead>
                  <tr className="text-center">
                    <th scope="col">Tarea</th>
                    <th scope="col">Estado</th>
                    <th scope="col">Completed_at</th>
                    <th scope="col">Eliminar</th>
                    <th scope="col">Ver</th>
                  </tr>
                </thead>
                <tbody>
                    {
                       useTasks ? useTasks.map(task =>{
                        return(
                        <tr key={task.id} className="text-center">
                            <th>{task.name}</th>
                            <th>{task.completed ? <i className="successIcon" onClick={ () => handleChangeStatus(task.id , task.completed) }> <FontAwesomeIcon icon={faCheckSquare}/> </i>: <i className="errorIcon" onClick={ () => handleChangeStatus(task.id , task.completed)}> <FontAwesomeIcon icon={faMinusSquare}/> </i> }</th>
                            <th>{task.completed_at}</th>
                            <th><i className="errorIcon" onClick={() => handleDeleteTask(task.id)}><FontAwesomeIcon icon={faTrashAlt}/></i></th>
                            <th><Link to={`/task/${task.id}`}><i><FontAwesomeIcon icon={faEye}/></i></Link></th>
                        </tr>
                        ) 
                       })
                       
                       : <tr></tr>
                    }
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Inicio;
