import axiosConfig from '../configs/axiosConfig'

export const logout = async () => {
    return await axiosConfig.post('api/logout')
                            .then(result => {
                                console.log(result);
                                if(!result.data.success){
                                    
                                    localStorage.removeItem('access_token');
                                    if (typeof window !== 'undefined') {
                                        window.location.href = "/";
                                   }
                                }
                                return true;
                            })
                            .catch(error => {throw new Error("Hubo un error al contactar al servidor")})
}

export const deleteTask = async (id) => {
    return await axiosConfig.delete(`api/tasks/${id}`)
                                .then(result => {
                                    return result.data
                                })
                                .catch(error => {throw new Error("Error al comunicarse con el servidor")});
}

export const updateTask = async (id , status) => {
    return await axiosConfig.put(`api/updateStatus/${id}` , {'completed' : status})
                                .then(result => {
                                    return result.data
                                })
                                .catch(error => {throw new Error("Error al comunicarse con el servidor")});
}

export const newTask = async (data) => {
    return await axiosConfig.post('api/tasks' , data)
                                .then(result => {
                                    return result.data
                                })
                                .catch(error => {throw new Error("Error al comunicarse con el servidor")});
}

export const getTasks = async (data) => {
    data = data || null;
  return await axiosConfig.get('api/tasks?search='+data)
        .then( result => {
            return result.data
        })
            .catch(error => window.location.reload());
}

export const getSingleTask = async (id) => {
    return await axiosConfig.get(`api/tasks/${id}`)
                    .then(result =>{
                        return result.data
                    })
                        .catch(error => {throw new Error("Error al comunicarse con el servidor")});
}

export const login = async (data) => {
    return await axiosConfig.post(`api/login` , data)
                        .then(result => {
                            if(result.status){
                                localStorage.setItem('access_token' , result.data.data.access_token);
                                window.location.reload();
                            }
                        })
                            .catch(error => {throw new Error("Error al comunicarse con el servidor")});
}

